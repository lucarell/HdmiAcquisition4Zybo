# HdmiAcquisition4Zybo

Supervisor: Renaud Pacalet

Students: Marco Lucarella, Enrico Gioia


# Table of content
* [License](#License)
* [Project Description](#Description)
* [Usage](#Usage)
* [Repository Content](#Content)
* [Project Details](#Details)
* [Future improvement](#TODO)


# <a name="License"></a>License
...

# <a name="Description"></a>Project Description
The goal is to design a digital hardware for HDMI acquisition that allows to manipulate each pixel and then print the output on the VGA interface.

As reference and start point, we used an open source [project](https://reference.digilentinc.com/zybo:hdmidemo) provided by Digilent.  
The entire work is made up of several modules which are implemented either on the FPGA logic or on the CPU. Among them there are:

* **HDMI Acquisition (dvi2rgb)** : module created by Digilent able to interpret the HDMI protocol. It decodes the video stream and outputs 24-bit
RGB video data along with the pixel clock and synchronization signals;
* **Clock Wizard** : multiply the pixel clock coming from the HDMI by a certain factor;
* **Format Converter In** : reorganizes the acquired video data and control signals to make them more usable;
* **Custom block** : empty module where put some custom VHDL;
* **Format Converter Out**: re-reorganizes the video data and control signals in the format used by the Digilent;
* **RGB to AXI**: module created by Digilent able to load the image on the RAM to be accessible from the CPU.


![Data flow](doc/pictures/MainView.png)

# <a name="Usage"></a>Usage

## Gather your materials

Begin by gathering the materials:

* Zybo board (Zynq-7000 Development Board)
* Xilinx Vivado® Design Suite: Design Edition
* Micro USB cable
* 5V 2.5A switching power supply
* VGA Cable to connect to a monitor
* Monitor with a VGA Input
* HDMI cable
* HDMI video source (most modern laptop computers work or a GoPro)

![Data flow](doc/pictures/ZyboDetails.png)

## Dependencies

The entire project is created using ```Vivado 2015.4 HL Design Edition```. Other versions, such as Vivado 2015.4 WebPack Edition, can also be used, but for them there will not be a full compatibility with the Digilent Zybo Board and so a lot of warnings will be detected. Moreover, if a different version from that suggested is used, the ```script/system.tcl``` file has to be modified by commenting the first lines of code where a check on the Vivado version used is performed.

It is possible to simulate the entire project by using the provided MakeFile. Please note that to do so ```Mentor Graphics ModelSim``` or ```ModelSim Altera``` is required.

## Add VHDL code to the project

By default, a VHD file ```hdl/CustomBlock.vhd``` is provided. It is possible to modify its content by writing some custom VHDL code and continue to the next step. If your project consist in more then one file the following steps have to be performed:
* copy all vhdl source files inside the ```hdl```folder;
* modify the file ```hdl/CustomBlock.vhd``` to make it the main entity;
* if the name of the top entity changes, modify it also inside the ```script/global_configuration.tcl``` file.

## Compile the source files (optional)
It is possible to compile the project using the MakeFile in the project root directory and by typing:

	$ make ms-all

This command will compile all the files inside the hdl folder checking if there are syntax errors.

## Create the Vivado project

To performs this step, use the MakeFile in the project root directory by typing:

	$ make proj-all

The command will create the IP blocks, create the project, start the synthesis  and open Vivado SDK needed for the last step.
It is possible to split the command and performs only some of its steps:

	# Create the IP blocks
	$ make ip-all

	# Create the Vivado project
	$ make proj-create

	# Run the synthesis
	$ make proj-syntesis

To open the project, launch the file ```build/proj/HdmiAcquisition4Zybo.xpr``` with Vivado GUI.

## Board connections and jumpers

![Data flow](doc/pictures/Connections.png)

* Plug one end of the HDMI cable into a video source and the other into the ZYBO HDMI port.
* Plug one end of your VGA cable into the VGA port of the ZYBO and the other into your VGA monitor.
* Apply power using the wall plug.

**Note:** It's important to make sure that your power supply jumper is switched from USB Power to Wall Power.

**Note:** It's important to make sure that your programmable jumper is switched to JTAG.

## Flash the FPGA and run the CPU program

After the synthesis phase, the MakeFile automatically opens Vivado SDK in order to flash the Zybo Board.
The SDK can be also run using the GUI:
* open the Vivado project;
* export the hardware using ```File > Export > Export Hardware...``` in the main menu;
* launch the SDK using ```File > Launch SDK``` in the main menu.

Inside the SDK the FPGA can be programmed using ```Xilinx Tools > Program FPGA```. If the bitestream is not automatically detected, it can be loaded  using the Browse button. The file is located into ```build/proj/HdmiAcquisition4Zybo.runs/impl_1/HdmiAcquisition4Zybo_wrapper.bit```.

Finally, the C project has to be ran on the Zybo CPU performing the following steps:
* Import the project inside the SDK:
	* ```File > Import...``` then ```General > Existing Project into Workspace```;
	* Select as root directory ```sdk``` and then import the two project inside;
* Run the C project as ```"Launch on Hardware (System Debugger)"``` mode.

## Interact with the CPU program

![UART menu](doc/pictures/UART_menu.png)

The CPU program opens a serial communication with the PC that allows to try some sample output, set the resolution and configure other parameters. To access these options, open a serial communication with:

	sudo picocom -b115200 -fn -pn -d8 -r -l /dev/ttyUSB1

# <a name="Content"></a>Repository Content

	.
	├── build                         File generated by the MakeFile
	├── constraints                   Vivado Project constraints for the synthesis
	├── doc                           Documentation files
	├── hdl                           VHDL source code
	│   └── CustomBlock-vhd           Block to substitute with your main entity
	├── repo                          Already compiled IP blocks
	├── scripts                       TCL scripts
	│   ├── create_ipblocks.tcl       Package the vhdl files in IP blocks
	│   ├── create_project.tcl        Create the vivado project
	│   ├── documentaion.tcl          Export the data blocks as image
	│   ├── synthesis.tcl             Run the synthesis and the implementation
	│   └── system.tcl                Description of the system and the connections
	├── sdk                           Project for the Digilent Zybo board CPU
	│   ├── videodemo                 Main CPU project
	│   └── videodemo_bsp             Library needed for the project
	├── Makefile                      Main makefile
	└── README.md                     This file



# <a name="Details"></a>Project Details

## Blocks overview

A complete scheme of the entire project is provided in the following figure:

![Data flow](doc/pictures/HdmiAcquisition4Zybo.svg.png)

Among all blocks, the critical ones are described below:
* **dvi2rgb**: this is the block that read the HDMI interface.
	* the input TMDS is the name of the HDMI interface dedicated for video;
	* the output DDC is an I2C interface used by the devices connected to the HDMI in order to detect the resolution supported by the Zybo. This interface allows the peripheral to read a ROM stored in this block that can be configured using the block options;
	* the output RGB is composed of several signals:
		* vid_pData is contains the information on the pixel color coded using one byte for each color in the order BRG;
		* vid_pHSync and vid_pVSync represet the vertical and orizzontal sync signals;
		* vid_pVDE indicated if the video is enabled or not;
	* the output PixelClk is the clock extracted from the HDMI and it depends on the resolution used;
	* the output aPixelClkLckd is set to one when the block finish to detect the PixelClk;
	* the optional output SerialClk can be enabled from the block configuration and is equal to the PixelClk but five time faster.
* **v_vid_in_axi4s**: this is the block that creates the AXI interface in order to transmit the pixel to the RAM.
	* the input vid_io_in was originally connected to the RGB port of dvi2rgb and has the same pin.


## Implemented blocks description

The interface created in this project allows to easily interact with the HDMI pixel flows using VHDL blocks.

The block ```FromatConverterIn.vhd``` reorganize the pixel information in order to have in output a signal that repreest the RGB pixel (```PixelRGB```) with 8 bit for each channel and ordered with Red as most significant byte and Blue as less significant byte.

This blocks also generate a ```PixelControl``` signal that contains these informations:
* bit 0: Horizontal sync;
* bit 1: Vertical sync;
* bit 2: Video enable;

This signal can be used do some check on the pixel received:
* the received pixel is valid if all the bit are one;
* the pixel row is finished if there is a falling edge on the horizontal sync bit;
* a new pixel row is started if there is a rising edge on the horizontal sync bit;
* the image is finished if there is a falling edge on the vertical sync bit;
* a new image is started if there is a rising edge on the horizontal sync bit;

The block ```FormatConverterOut``` restore the previous configuration in order to not create any problem with the subsequent blocks.

## Clocks
The clock provided to the custom block is generated using a ```Clock Wizard``` that takes as input the pixel clock, so this clock is synchronous to it but is not synchronous to the CPU Clock.

The clock used by the custom block is 8 time faster than the pixel clock only if the HDMI is configured with a resolution of 640x480 px so a pixel clock of 25 MHz. This means that the circuit inside the custom block must be configured to run at 200 MHz and to be ready to accept a new pixel every 8 clock cycle when the ```start``` signal rises.

With an input image that has a different resolution the pixel clock changes so the circuit and the ```Clock Wizard``` has to be configured differently.

If the circuit violate the time constrain it is possible to change the clock frequency modifying the ```Clock Wizard```, increase the hardware used or use a pipelined approach.

## CPU project

The CPU project provided different features:
* initialize the frames: inside the RAM are stored 3 frames, using the UART interface is possible to select what you want to write in each frame and witch frame do you want to visualize on the VGA;
* attach a callback on the HDMI status: every time the HDMI changes status a callback is executed to update the UART menu and if the HDMI is detected it starts automatically the streaming on the VGA;
* sample image: there are two sample image that is possible to show on the VGA using the UART menu. Other sample images can be added modifying the C code;
* adjust the resolution: using the UART menu is possible to select the resolution to use on both HDMI and VGA.
* capture a frame: is possible to capture a frame of the HDMI stream and do some software modification. The color inversion filter is already implemented and can be enabled from the UART menu.

# <a name="TODO"></a>Future improvement

* Add the resolution 640x480 to the dvi2rgb block
	* Create a new ROM snapshot similar to the ```repo/digilent/ip/dvi2rgb_v1_6/src/720p_edid.txt``` inside the same folder;
	* Add a new option for this ROM in the IP block configuration and rebuild it.
* Reduce the size of the project inside the FPGA
	* Removing the Swithc, Led and Buttons interface;
	* Reduce the frame stored in memory from 3 to 1;

	* Add the possibility to remove the hardware dedicated to the VGA ouptut in order to reduce the FPGA usage. This choise has to be selected inside the script ```global_configuration.tcl```.
* Add an option in the MakeFile in order to load an run the program without open the SDK GUI.
* Create a new project that doesn't output on the VGA but only acquire from HDMI.
* Implement a reset signal.
