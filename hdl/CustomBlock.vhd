library IEEE;
use IEEE.std_logic_1164.ALL;

entity CustomBlock is
	Port (
		-- Control
		clk 							: in  std_ulogic;
		start							: in  std_ulogic;
		-- Data in
		PixelRGB_in				: in  std_ulogic_vector(23 downto 0);
		PixelControl_in		: in  std_ulogic_vector( 2 downto 0);
		-- Data out
		PixelRGB_out			: out std_ulogic_vector(23 downto 0);
		PixelControl_out	: out std_ulogic_vector( 2 downto 0)
	);
end CustomBlock;

architecture Behavioural of CustomBlock is
begin
	flip_flops: process(clk)
		begin
			if rising_edge(clk) then
				if start = '1' then
					PixelRGB_out <= PixelRGB_in;
					PixelControl_out <= PixelControl_in;
				end if;
			end if;
		end process flip_flops;

end Behavioural;
