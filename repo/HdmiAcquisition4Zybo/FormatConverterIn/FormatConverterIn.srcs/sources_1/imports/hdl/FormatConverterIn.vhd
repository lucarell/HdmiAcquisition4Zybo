library IEEE;
use IEEE.std_logic_1164.ALL;

entity FormatConverterIn is
	Port (
		-- Control
		clk 					: in  std_ulogic;
		pixel_clk			: in  std_ulogic;
		locked				: in  std_ulogic;
		done					: out std_ulogic;
		-- Data in
		PixelData			: in  std_ulogic_vector(23 downto 0);
		HSync					: in  std_ulogic;
		VSync					: in  std_ulogic;
		vid_pVDE			: in  std_ulogic;
		-- Data out
		PixelRGB			: out std_ulogic_vector(23 downto 0);
		PixelControl	: out std_ulogic_vector( 2 downto 0)
	);
end FormatConverterIn;

architecture Structural of FormatConverterIn is

	signal prev_val_pixel_clk : std_ulogic;

begin
	flip_flops: process(clk)
		begin
			if rising_edge(clk) then
				if locked = '1' and pixel_clk = '1' and prev_val_pixel_clk = '0' then
					PixelRGB(23 downto 16) <= PixelData(23 downto 16); -- Red
					PixelRGB(15 downto  8) <= PixelData( 7 downto  0); -- Green
					PixelRGB( 7 downto  0) <= PixelData(15 downto  8); -- Blue
					PixelControl(0) <= HSync;
					PixelControl(1) <= VSync;
					PixelControl(2) <= vid_pVDE;
					done <= '1';
				else
					done <= '0';
				end if;

				prev_val_pixel_clk <= pixel_clk;
			end if;
		end process flip_flops;

end Structural;
