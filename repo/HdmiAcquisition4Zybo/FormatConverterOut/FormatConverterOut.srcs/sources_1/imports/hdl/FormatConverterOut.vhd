library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.numeric_std.ALL;

entity FormatConverterOut is
	Port (
		-- Control
		pixel_clk					: in  std_ulogic;
		-- Data in
		PixelRGB					: in  std_ulogic_vector(23 downto 0);
		PixelControl			: in  std_ulogic_vector( 2 downto 0);
		-- Data out
		PixelData					: out std_ulogic_vector(23 downto 0); -- RBG
		HSync							: out std_ulogic;
		VSync							: out std_ulogic;
		vid_active_video	: out std_ulogic
	);
end FormatConverterOut;

architecture Structural of FormatConverterOut is
	begin
		flip_flops: process(pixel_clk)
			begin
				if rising_edge(pixel_clk) then
					PixelData(23 downto 16) <= PixelRGB(23 downto 16); -- Red
					PixelData( 7 downto  0) <= PixelRGB(15 downto  8); -- Green
					PixelData(15 downto  8) <= PixelRGB( 7 downto  0); -- Blue
					HSync <= PixelControl(0);
					VSync <= PixelControl(1);
					vid_active_video <= PixelControl(2);
				end if;
			end process flip_flops;
	end Structural;
