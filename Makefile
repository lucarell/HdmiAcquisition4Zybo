#
# Copyright (C) Telecom ParisTech
#
# This file must be used under the terms of the CeCILL. This source
# file is licensed as described in the file COPYING, which you should
# have received as part of this distribution. The terms are also
# available at:
# http://www.cecill.info/licences/Licence_CeCILL_V1.1-US.txt
#

#############
# Variables #
#############

# General purpose
DEBUG	?= 1
SHELL	:= /bin/bash

ifeq ($(DEBUG),0)
OUTPUT	:= &> /dev/null
else ifeq ($(DEBUG),1)
OUTPUT	:= > /dev/null
else
OUTPUT	:=
endif

rootdir		:= $(realpath .)
BUILD			:= build
HDLDIR		:= hdl
HDLSRCS		:= $(wildcard $(HDLDIR)/*.vhd)
SCRIPTS		:= scripts

# Mentor Graphics Modelsim
MSBUILD			:= $(BUILD)/ms
MSCONFIG		:= $(MSBUILD)/modelsim.ini
MSLIB				:= vlib
MSMAP				:= vmap
MSCOM				:= vcom
MSCOMFLAGS	:= -ignoredefaultbinding -nologo -quiet -2008
MSSIM				:= vsim
MSSIMFLAGS	:= -voptargs="+acc"
MSTAGS			:= $(patsubst $(HDLDIR)/%.vhd,$(MSBUILD)/%.tag,$(HDLSRCS))

# Xilinx Vivado ip blocks
VVMODE			?= batch
VIVADO			:= vivado
IPBUILD			:= $(BUILD)/ip
IPSCRIPT		:= $(SCRIPTS)/create_ipblocks.tcl
VIVADOFLAGS	:= -mode $(VVMODE) -notrace -source $(IPSCRIPT) -tempDir /tmp -journal $(IPBUILD)/vivado.jou -log $(IPBUILD)/vivado.log -tclargs $(rootdir) $(IPBUILD)
IPIMPL			:= $(IPBUILD)/top.runs/impl_1
IPBIT				:= $(IPIMPL)/top_wrapper.bit

# Xilinx Vivado Porject
PROJBUILD			:= $(BUILD)/proj
PROJSCRIPT		:= $(SCRIPTS)/create_project.tcl
PROJFLAGS			:= -mode $(VVMODE) -notrace -source $(PROJSCRIPT) -tempDir /tmp -journal $(PROJBUILD)/vivado.jou -log $(PROJBUILD)/vivado.log -tclargs $(rootdir) $(PROJBUILD)
PROJSYNSCRIPT	:= $(SCRIPTS)/synthesis.tcl
PROJSYNFLAGS	:= -mode $(VVMODE) -notrace -source $(PROJSYNSCRIPT) -tempDir /tmp -journal $(PROJBUILD)/vivado.jou -log $(PROJBUILD)/vivado.log -tclargs $(rootdir) $(PROJBUILD)

# Messages
define HELP_message
make targets:
  make help         print this message (default target)
  make ms-all       compile all VHDL source files with Modelsim ($(MSBUILD))
  make ms-clean     delete all files and directories automatically created by Modelsim
  make ip-all       synthesize ip blocks with Vivado ($(IPBUILD))
  make ip-clean     delete all files and directories automatically created by Vivado ip blocks
  make proj-all     create and synthetize the Vivado projcet ($(PROJBUILD))
  make proj-create  create the Vivado projcet ($(PROJBUILD))
  make proj-clean   delete all files and directories automatically created by Vivado project
  make clean        delete all automatically created files and directories

directories:
  hdl sources              ./$(HDLDIR)
  build                    ./$(BUILD)
  Modelsim build           ./$(MSBUILD)
  Vivado ip blocks build   ./$(IPBUILD)
  Vivado project build     ./$(PROJBUILD)

customizable make variables:
  DEBUG   debug level: 0=none, 1: some, 2: verbose ($(DEBUG))
  VVMODE  Vivado running mode (gui, tcl or batch) ($(VVMODE))
endef
export HELP_message

# Help
help:
	@echo "$$HELP_message"

#################
#  Dependencies #
#################
# Example
# $(MSBUILD)/Custom_block1.tag: $(MSBUILD)/Custom_block2.tag


################
# Make targets #
################

# Mentor Graphics Modelsim
ms-all: $(MSTAGS)

$(MSTAGS): $(MSBUILD)/%.tag: $(HDLDIR)/%.vhd
	@echo '[MSCOM] $<' && \
	cd $(MSBUILD) && \
	$(MSCOM) $(MSCOMFLAGS) $(rootdir)/$< && \
	touch $(rootdir)/$@

$(MSTAGS): $(MSCONFIG)

$(MSCONFIG):
	@echo '[MKDIR] $(MSBUILD)' && \
	mkdir -p $(MSBUILD) && \
	cd $(MSBUILD) && \
	$(MSLIB) .work $(OUTPUT) && \
	$(MSMAP) work .work $(OUTPUT)

ms-clean:
	@echo '[RM] $(MSBUILD)' && \
	rm -rf $(MSBUILD)

# Xilinx Vivado
ip-all: $(IPBIT)

$(IPBIT): $(HDLSRCS) $(IPSCRIPT)
	@echo '[VIVADO] $(IPSCRIPT)' && \
	mkdir -p $(IPBUILD) && \
	$(VIVADO) $(VIVADOFLAGS)

$(SYSDEF):
	@$(MAKE) ip-all

ip-clean:
	@echo '[RM] $(IPBUILD)' && \
	rm -rf $(IPBUILD)

# Vivado project

proj-all: ip-all proj-create proj-syntesis

proj-create: $(HDLSRCS) $(PROJSCRIPT)
	@echo '[PROJECT] $(PROJSCRIPT)' && \
	mkdir -p $(PROJBUILD) && \
	$(VIVADO) $(PROJFLAGS)

proj-syntesis:
	@echo '[PROJECT] $(PROJSYNSCRIPT)' && \
	mkdir -p $(PROJBUILD) && \
	$(VIVADO) $(PROJSYNFLAGS)

proj-clean:
	@echo '[RM] $(PROJBUILD)' && \
	rm -rf $(PROJBUILD)

# Full clean
clean: ms-clean ip-clean proj-clean
	@echo '[RM] $(BUILD)' && \
	rm -rf $(BUILD)
