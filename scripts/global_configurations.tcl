# Project configuration and properties

# Project name
set proj_name "HdmiAcquisition4Zybo"

# Design name
set design_name "HdmiAcquisition4Zybo"

# Library vendor
set vendor "www.eurecom.fr"

# Top entity name
set top_entity "CustomBlock"

# Board description
set part "xc7z010clg400-1"
set brd_part "digilentinc.com:zybo:part0:1.0"
