# Creates the documentations files

set origin_dir [file normalize [file dirname [info script]]]/..
source $origin_dir/scripts/global_configurations.tcl
set output_file_name $origin_dir/doc/BlockDiagram

# Export the board image for documentation purposes
open_bd_design [concat $origin_dir/build/proj/$proj_name.srcs/sources_1/bd/$proj_name/$proj_name.bd]
write_bd_layout -format pdf -orientation landscape "$output_file_name .pdf"
write_bd_layout -format svg "$output_file_name .svg"

puts "**************************************"
puts "******* Documentation exported *******"
puts "**************************************\n"
