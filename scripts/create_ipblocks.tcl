proc usage {} {
	puts "usage: vivado -mode batch -source <script> -tclargs <origin_dir> <builddir>"
	puts "  <origin_dir>:  absolute path of project root directory"
	puts "  <builddir>: absolute path of build directory"
	exit -1
}

if { $argc == 2 } {
	set origin_dir [lindex $argv 0]
	set builddir [lindex $argv 1]
} else {
	usage
}

cd $builddir
set hdldir $origin_dir/hdl
source $origin_dir/scripts/global_configurations.tcl

################################
## Create FormatConverterIn IP #
################################
#create_project -part $part -force FormatConverterIn FormatConverterIn
#add_files $hdldir/FormatConverterIn.vhd
#import_files -force -norecurse
#ipx::package_project -root_dir FormatConverterIn -vendor www.eurecom.fr -library HdmiAcquisition4Zybo -force FormatConverterIn
#close_project
#
#################################
## Create FormatConverterOut IP #
#################################
#create_project -part $part -force FormatConverterOut FormatConverterOut
#add_files $hdldir/FormatConverterOut.vhd
#import_files -force -norecurse
#ipx::package_project -root_dir FormatConverterOut -vendor www.eurecom.fr -library HdmiAcquisition4Zybo -force FormatConverterOut
#close_project

#########################
# Create $top_entity IP #
#########################
create_project -part $part -force $top_entity $top_entity
set src_files [glob $hdldir/*.vhd]
add_files $src_files
import_files -force -norecurse
ipx::package_project -root_dir $top_entity -vendor $vendor -library $proj_name -force $top_entity
close_project
