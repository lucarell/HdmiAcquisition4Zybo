# Launch synthesis imlementation and bytestream generation

set origin_dir [file normalize [file dirname [info script]]]/..
source $origin_dir/scripts/global_configurations.tcl
set project_path $origin_dir/build/proj
set project_file [concat $project_path/$proj_name].xpr

open_project $project_file

# Documentation export
#source $origin_dir/scripts/documentation.tcl

# Synthesis
set sys_run [get_runs synth*]
launch_runs $sys_run -jobs 2
wait_on_run $sys_run
open_run $sys_run
puts "***********************************************************************"
puts "***** Synthesis terminated succesfully *****"
puts "***********************************************************************\n"

# Implementation
set impl_run [get_runs impl*]
reset_run $impl_run
set_property STEPS.WRITE_BITSTREAM.ARGS.BIN_FILE true $impl_run
launch_runs -to_step write_bitstream $impl_run -jobs 2
wait_on_run $impl_run
puts "***********************************************************************"
puts "***** Implementation terminated succesfully *****"
puts "***********************************************************************\n"

# Open SDK
file mkdir [concat $project_path/$proj_name.sdk]
file copy -force [concat $project_path/$proj_name.runs/$impl_run/$proj_name]_wrapper.sysdef [concat $project_path/$proj_name.sdk/$proj_name]_wrapper.hdf
launch_sdk -workspace [concat $project_path/$proj_name.sdk] -hwspec [concat $project_path/$proj_name.sdk/$proj_name]_wrapper.hdf
